import React, { Component } from 'react'
import {Link} from "react-router-dom";

const Option = props => {
    const options = props.options.map((row, index) => {
        return (
            <a className="dropdown-item" href="/">{row}</a>
        )});
    return <div>{options}</div>
};

const Filter = props => {
    const filters = props.filters.map((row, index) => {
        return (

            <div className="dropdown col-2">
                <button className="btn btn-default dropdown-toggle" type="button" id="dropdownMenuButton"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {row.name}
                </button>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <Option options={row.options}/>
                </div>
            </div>

        )});
    return <div className="row mb-4">{filters}</div>
};




class UpdateEquipment extends Component {

    state = {
        equip: {title: "Рентген",
                    produced: "Китай, 2015",
                    producer: "Неискусственный интеллект",
                    id: 1},
        filters: [
            {
                name: "Тип обслуживания",
                options: ["Периодическое ТО", "Техническое диагностирование", "Ремонт МИ", "Внеплановое ТО", "Обновление ПО", "Контроль технического состояния", "Монтаж/демонтаж и надалка МИ"]
            },
            {
                name: "Обслуживающая фирма",
                options: ["Неискусственный интеллект", "ЧинимДажеЛыжи", "Хорошая компания"]
            }
        ]


    };

    render() {
        const id = this.props.match.params.id;
        const row = this.state.equip;
        return (
            <div>
                <div className="row mt-3">
                    <div className="col-12">
                        <h3>Обновить данные оборудования</h3>
                    </div>
                </div>
                <div className="row">
                    <div className="col-4">
                        <span><b>Название: {row.title}</b></span>
                        <br/>
                        <span className="mt-3">Произведен: {row.produced}</span>
                        <br/>
                        <span className="">Производитель: {row.producer}</span>
                        <br/>
                    </div>
                </div>

                <div className="row mt-5">
                    <div className="col-6">
                        <h3>Добавить заявку на обслуживание</h3>
                        {/*<Filter filters={this.state.filters}/>*/}
                        <form className="form">
                            <input style={{width: 90+"%"}} className="form-control" type="search" placeholder="Причина обслуживания" aria-label="Search"/>
                            <button style={{width: 90+"%"}} className="btn btn-outline-primary grad-btn mt-2" type="submit">Отправить заявку</button>
                        </form>
                    </div>
                    <div className="col-6">
                        <h3>Добавить историю использования</h3>
                        <form className="form">
                            <input style={{width: 90+"%"}} className="form-control" type="search" placeholder="Количество раз" aria-label="Search"/>
                            <button style={{width: 90+"%"}} className="btn btn-outline-primary grad-btn mt-2" type="submit">Добавить</button>
                        </form>
                    </div>
                </div>

            </div>)
    }
}

export default UpdateEquipment