import axios from 'axios'
axios.defaults.withCredentials = true;

export async function getProductDetails(id) {
    return axios.get(`http://ihunter.ngrok.connect.club/equipment/${id}`)
        .then(res => res.data)
}

export async function getProductsForSale(s) {
    return axios.get(`http://ihunter.ngrok.connect.club/equipment`)
        .then(res => res.data)
}
