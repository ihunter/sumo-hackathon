import {Component} from "react";
import React from "react";
import { Link } from "react-router-dom";
import { getProductDetails } from './Api'


const Nav = props => {
    return (
        <nav aria-label="breadcrumb">
            <ol className="breadcrumb" id="breadcrumb">
                <li className="breadcrumb-item"><Link to="/">Главная</Link></li>
                <li className="breadcrumb-item"><Link to="/">Объявления о продаже</Link></li>
                <li className="breadcrumb-item"><a href="#">Лучевая терапия</a></li>
                <li className="breadcrumb-item active" aria-current="page">Портативный дентальный рентген</li>
            </ol>
        </nav>
    )
};

const ImageBlock = props => {
    const product = props.product;
    return (
        <div className="col-7 mb-07" >
            <div className="row mb-2 no-gutters">
                <img alt="" style={{width: 100+"%", height: "300px", "object-fit":"cover"}} src={product.img}/>
            </div>
            <div className="row no-gutters" style={{display: "flex", "justify-content": "space-after", "flex-direction": "row"}}>
                {product.additionalImages.map(img =>
                    <img alt="" style={{width: 24+"%", height: "90px", "object-fit":"cover"}} src={img} key={img}/>
                )}
            </div>
        </div>
    )
};

const Description = props => {
    const product = props.product;
    return (
        <div className="col-5 mb-0">
            <div className="pb-2">
                <span>Страна происхождения:</span>
                <span className="float-right">{product.manufacturer_country}</span>
                <br/>
            </div>
            <div className="pb-2">
                <span>Текущий владелец:</span>
                <span className="float-right">{product.ownerName}</span>
                <br/>
            </div>
            <div className="pb-2">
                <span>Год производства:</span>
                <span className="float-right">{product.production_year}</span>
                <br/>
            </div>
            <div className="pb-2">
                <span>Состояние:</span>
                <span className="float-right">{product.is_new ? "Новый" : "Б/у"}</span>
            </div>
            <div className="bottom">
                <button className="btn btn-primary grad-btn full-width mb-2">Купить</button>
                <button className="btn btn-outline-primary full-width mb-2">Контакты продавца</button>
                <button className="btn btn-outline-primary full-width mb-0">Заказать оценку</button>
            </div>

        </div>
    )
};

const FullInfo = props => {

    const product = props.product;
    const url_history = `/product_detail/${props.productId}/history/`;
    const url_detail = `/product_detail/${props.productId}/detail/`;

    return (
        <div className="card" style={{width: 100+"%"}}>
            <div className="card-header">
                <ul className="nav nav-tabs card-header-tabs">
                    <li className="nav-item">
                        <Link to={url_history} className={"nav-link" + (props.tab !== 'detail' ? ' active' : '')}>История обслуживания</Link>
                    </li>
                    <li className="nav-item">
                        <Link to={url_detail} className={"nav-link" + (props.tab === 'detail' ? ' active' : '')}>Характеристики</Link>
                    </li>
                </ul>
            </div>
            {props.tab !== "detail" &&
                <History history={product.history}/>}
            {props.tab === "detail" &&
                <FullDescription product={product}/>}
        </div>
    )
};

const FullDescription = props => {
    console.log(props.product.description)
    return <>
        <li className="list-group-item card-text">
            {props.product.description.split("\n").map(x => <><span>{x}</span><br/></>)}
        </li>
    </>
}

const History = props => {
    const history = props.history.map((row, index) => {

        let opHuman = row.op;
        switch (row.op) {
            case 'create': opHuman = 'Оборудование добавлено в систему'; break;
            case 'change_description': opHuman = 'Изменено описание'; break;
            case 'service_request': opHuman = 'Заявка на сервисное обслуживание'; break;
            case 'repair_request': opHuman = 'Заявка на ремонт'; break;
            default:
        }
        return (
            <li className="list-group-item">
                {row.reason ?
                    <><span style={{height: "24px", "font-size": "16px", "font-weight": "bold"}} >{row.reason}</span><br/></>
                    : null}

                <span>{opHuman}</span>
                <br/>
                <span className="text-muted mt-3">{new Date(+row.date * 1000).toLocaleDateString()}</span>
                <br/>
                <div className="row no-gutters" style={{display: "flex", "justify-content": "space-after", "flex-direction": "row"}}>
                {(row.opPhotos || []).map(img =>
                    <img alt="" style={{width: "60px", height: "60px", "object-fit":"cover"}} src={img} key={img}/>
                )}
                </div>

            </li>
        )
    });
    return <ul className="list-group list-group-flush">{history}</ul>
};


class ProductDetail extends Component {
    state = {};

    componentDidMount() {
        getProductDetails(this.props.match.params.id)
            .then(product => this.setState({ product }))
    }

    render() {
        if (!this.state.product) {
            return <div/>
        }
        const product  = this.state.product;

        return (
            <div>
                <div className="row">
                    <div className="col-12">
                        <Nav/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <h2>{product.title}</h2>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <p>{product.current_location}, {new Date(+product.date * 1000).toLocaleDateString()}</p>
                    </div>
                </div>
                <div className="row mb-0">
                    <ImageBlock product={product}/>
                    <Description product={product}/>
                </div>
                <div className="row no-gutters mt-3">
                    <FullInfo product={product} productId={this.props.match.params.id} tab={this.props.match.params.tab}/>
                </div>
            </div>
        )
    }
}

export default ProductDetail;
