import React, { Component } from 'react'
import { Link } from "react-router-dom";
import LogoSumo from './static/LogoSumo.png'

class Header extends Component {
    render() {

        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container">
                    <Link to='/' className="navbar-brand">
                        <img src={LogoSumo}/>
                    </Link>
                    <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                        <ul className="navbar-nav">
                            <li className="nav-item active">
                                <Link to="/profile/equipment" className="nav-link" href="#">Иван Иванов<span className="sr-only">(current)</span></Link>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Клиника 53</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        )
    }
}

export default Header;