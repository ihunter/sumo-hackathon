import React, { Component } from 'react'
import {Link} from "react-router-dom";

const SearchForm = props => {
    return (
        <form className="form-inline">
            <input className="form-control" style={{width: 85+"%"}} type="search" placeholder="Search" aria-label="Search"/>
            <button className="btn btn-outline-primary grad-btn" style={{width: 15+"%"}} type="submit">Search</button>
        </form>
    )
};

const Option = props => {
    const options = props.options.map((row, index) => {
        return (
            <a className="dropdown-item" href="/">{row}</a>
        )});
    return <div>{options}</div>
};

const Filter = props => {
    const filters = props.filters.map((row, index) => {
        return (

            <div className="dropdown col-2">
                <button className="btn btn-default dropdown-toggle" type="button" id="dropdownMenuButton"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {row.name}
                </button>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <Option options={row.options}/>
                </div>
            </div>

        )});
    return <div className="row mb-4">{filters}</div>
};


const Product = props => {
    const rows = props.products.map((row, index) => {
        const url_detail = "product_detail/"+row.id+"/history";
        const url_owner= "company_detail/".concat(row.owner);
        return (
            <div key={row.id} className="card mt-2 mb-2" style={{height: "150px"}}>
                <div className="row">
                    <div className="col-4">
                        <img className="img-responsive" src={row.img} style={{width: 100+"%", height: 100+"%", "object-fit":"cover"}}/>
                    </div>
                    <div className="col">
                        <div className="card-body ml-0 pl-0" style={{height: 100+"%"}}>
                            <h5 className="card-title">
                                <Link to={url_detail} style={{color: "black", "text-decoration": "none"}}>
                                    {row.title}
                                </Link>

                            </h5>
                            <div className="pb-0 mb-0 pt-5">
                                <Link to={url_detail} className="card-link">
                                    Цена: {row.price}  {row.currency}
                                </Link>
                                <Link to={url_owner} className="card-link float-right">Связаться с продавцом</Link>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    });

    return <div>{rows}</div>
};

class ProductList extends Component {

    render() {
        const { products } = this.props;
        const { filters } = this.props;
        return (
            <div>
                <div className="row mt-3 mb-3">
                    <div className="col-12">
                        <SearchForm/>
                    </div>
                </div>

                <Filter filters = {filters} />

                <Product products={products}/>

            </div>

        )
    }
}

export default ProductList
