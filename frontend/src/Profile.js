import React, { Component } from 'react'
import {Link} from "react-router-dom";

const FullInfo = props => {

    console.log(props);

    return (
        <div className="container">
            {props.tab === "equipment" &&
            <div className="card" style={{width: 100+"%"}}>
                <div className="card-header">
                    <ul className="nav nav-tabs card-header-tabs">
                        <li className="nav-item">
                            <Link to="/profile/equipment" className="nav-link active">Оборудование</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/profile/sales" className="nav-link">Объявления</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/profile/services" className="nav-link">Заявки на ремонт</Link>
                        </li>
                    </ul>
                </div>
                <Equipment equipment={props.equipment}/>
            </div>
            }
            {props.tab === "sales" &&
            <div className="card" style={{width: 100+"%"}}>
                <div className="card-header">
                    <ul className="nav nav-tabs card-header-tabs">
                        <li className="nav-item">
                            <Link to="/profile/equipment" className="nav-link">Оборудование</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/profile/sales" className="nav-link active">Объявления</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/profile/services" className="nav-link">Заявки на ремонт</Link>
                        </li>
                    </ul>
                </div>
                <Sales sales={props.sales}/>
            </div>
            }
            {props.tab === "services" &&
            <div className="card" style={{width: 100+"%"}}>
                <div className="card-header">
                    <ul className="nav nav-tabs card-header-tabs">
                        <li className="nav-item">
                            <Link to="/profile/equipment" className="nav-link">Оборудование</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/profile/sales" className="nav-link">Объявления</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/profile/services" className="nav-link active">Заявки на ремонт</Link>
                        </li>
                    </ul>
                </div>
                <Services services={props.services}/>
            </div>
            }

        </div>
    )
};

const Sales = props => {
    console.log(props.sales);
    const sales = props.sales.map((row, index) => {

        return (
            <li className="list-group-item">
                <span><b>{row.title}, {row.price} {row.currency}</b></span>
                <br/>
                <span className="text-muted mt-3">{row.produced}</span>
                <br/>
                <span style={{height: "24px", "font-size": "16px"}} className="">{row.status}</span>
                <br/>

            </li>
        )
    });
    return <ul className="list-group list-group-flush">{sales}</ul>
};

const Services = props => {
    console.log(props.services);
    const services = props.services.map((row, index) => {

        return (
            <li className="list-group-item">
                <span><b>{row.date} {row.title}</b></span>
                <br/>
                <span className="text-muted mt-3">Тип работы: {row.work_type}</span>
                <br/>
                <span className="">Примерная дата завершения: {row.end_date}</span>
                <br/>
                <span className="">Сервисер: {row.servicer}</span>
                <br/>
                <span className="">Неисправность: {row.what_is_wrong}</span>
                <br/>
                <Link className="card-link" to={'/'}>Подтвердить починку</Link>
                <Link className="card-link" to={'/'}>Отменить заявку</Link>
            </li>

        )
    });
    return <ul className="list-group list-group-flush">{services}</ul>
};



const Equipment = props => {
    console.log(props);
    const equipment = props.equipment.map((row, index) => {

        const update_url = "/update_equipment/"+row.id;

        return (
                <li className="list-group-item">
                    <span><b>{row.title}</b></span>
                    <button className="btn btn-primary grad-btn float-right">
                        <Link style={{color: "white"}} to={update_url}>Обновить данные</Link>
                    </button>
                    <br/>
                    <span className="text-muted mt-3">{row.produced}</span>
                    <br/>
                    <span style={{height: "24px", "font-size": "16px"}} className="">{row.status}</span>
                    <br/>
                </li>
        )
    });
    return <ul className="list-group list-group-flush">{equipment}</ul>
};


class Profile extends Component {

    state = {
        "equipment": [
                {title: "Рентген",
                produced: "Китай, 2015",
                status: "Неисправности отсутствуют",
                id: 1},
                {title: "Томограф",
                produced: "Россия, 2010",
                status: "В ремонте",
                id: 2},
                {title: "УЗИ-комплекс",
                produced: "Россия, 2008",
                status: "Неисправности отсутствуют",
                id: 3}
        ],
        "sales": [
                {title: "Томограф",
                    price: "30000",
                    currency: "руб",
                    produced: "Китай, 2005",
                    status: "Неисправности отсутсвуют"
                }
        ],
        "services": [
            {title: "Заявление на починку портативного томографа Метатрон 3000",
            date: "09.06.2019",
            work_type: "Ремонт МИ",
            end_date: "12.06.2019",
            servicer: "ЧинимДажеЛыжи",
            what_is_wrong: "пугает девочек, а должен дарить цветы"},
            {title: "Заявление на починку рентгена Рентген ABC",
                date: "08.06.2019",
                work_type: "Ремонт МИ",
                end_date: "21.09.2019",
                servicer: "Починим Ренгтен!",
                what_is_wrong: "рентген не проходит"}

        ]
    };



    render() {
        const tab = this.props.match.params.tab;
        console.log(tab);
        return(
           <div>
               <div className="row mt-4">
                   <div className="col-6">
                       <h5>Профиль пользователя</h5>
                       <span>Электронная почта: ivan@ya.ru</span>
                       <br/>
                       <span>ФИО: Иванов Иван Иванович</span>
                       <br/>
                       <span>Телефон: +79111111111</span>
                   </div>

                   <div className="col-6">
                       <h5>Организация</h5>
                       <span>Название: Клиника 53</span>
                       <br/>
                       <span>Адрес: Санкт-Петербург, Большой проспект П.С. 13</span>
                       <br/>
                       <span>Должность в организации: врач</span>
                   </div>
               </div>
               <div className="row mt-4">
                   <FullInfo equipment={this.state.equipment} sales={this.state.sales} services={this.state.services} tab={tab}/>
               </div>
           </div>
        )

    }
}

export default Profile
