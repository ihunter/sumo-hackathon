import React, {Component} from 'react';

import './App.css';
import Header from './Header'
import ProductList from './ProductList'
import ProductDetail from './ProductDetail'
import History from './ProductDetail'

import {
    Route,
    Switch
} from "react-router-dom";
import Profile from "./Profile";
import { getProductsForSale } from './Api';
import UpdateEquipment from "./UpdateEquipment";

class App extends Component {
    state = {
        products: [],

        filters: [
            {
                "name": "Категория",
                "filter-size": "30px",
                "options": [
                    "Стоматология",
                    "Томография",
                    "Рентген"
                ]
            },
            {
                "name": "Год",
                "filter-size": "20px",
                "options": [
                    "1998",
                    "2006",
                    "2015"
                ]
            },
            {
                "name": "Гарантия",
                "filter-size": "40px",
                "options": [
                    "Есть",
                    "Нет",
                ]
            },
            {
                "name": "Б/У",
                "filter-size": "30px",
                "options": [
                    "Да",
                    "Нет"
                ]
            }
        ]

    };

    componentDidMount() {
        getProductsForSale()
            .then(products => {
                this.setState({products})
            })
    }

    render() {

      const products = this.state.products;
      const filters = this.state.filters;

        return (
            <div className="container-fluid">
                <Header/>

                <div className="container mb-5" >
                    <Switch>
                        <Route exact path="/" component={ ()=> <ProductList products={products} filters={filters}/>} />
                        <Route path="/product_detail/:id/:tab" component={ProductDetail} />
                        <Route path="/history/:id" component={History} />
                        <Route exact path="/update_equipment/:id" component={UpdateEquipment} />
                        <Route path="/profile/:tab" component={Profile} />

                    </Switch>

                </div>

            </div>
        );
    }
}

export default App;
