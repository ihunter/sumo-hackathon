from bigchaindb_driver import BigchainDB
from pymongo import MongoClient
import time

mongo_client = MongoClient('mongodb://localhost:27017/')
bdb = mongo_client.bigchain
blockchain = BigchainDB('http://localhost:9984')

class AppException(Exception):
    def __init__(self, errorCode, message):
        self.errorCode = errorCode
        super(AppException, self).__init__(message)


class Equipment:
    @classmethod
    def create(cls, user, name, serialNo, additionalMetadata):
        assert name
        assert serialNo

        metadata = {
            "date": time.time(),
            "type": "equipment",
            "owner": user['public_key'],
            "status": "inuse",
            **additionalMetadata
        }
        t = blockchain.transactions.prepare(asset={
                    "data": {
                        "type": "equipment",
                        "name": name,
                        "serialNo": serialNo
                    }
                },
                metadata=metadata,
                signers=user['public_key'])
        t = blockchain.transactions.fulfill(t, user['private_key'])
        signed_tx = blockchain.transactions.send_commit(t)
        return {
            'equipmentId': signed_tx['id'],
            'transactionId': signed_tx['id'],
        }

    @classmethod
    def is_transaction_commited(cls, transactionId):
        assert transactionId
        return blockchain.blocks.get(txid=transactionId) != None

    @classmethod
    def update_metadata(cls, user, equipmentId, metadataChanges):
        transaction = cls.get_last_transaction(equipmentId)
        actualMetadata = cls.get_actual_metadata(transaction)

        output_index = 0
        output = transaction['outputs'][output_index]
        metadata = {**actualMetadata, **metadataChanges}
        print(transaction['id'])
        t = blockchain.transactions.prepare(
            operation='TRANSFER',
            asset={
                'id': equipmentId,
            },
            metadata=metadata,
            inputs={
                'fulfillment': output['condition']['details'],
                'fulfills': {
                    'output_index': output_index,
                    'transaction_id': transaction['id'],
                },
                'owners_before': output['public_keys'],
            },
            recipients=user['public_key'])
        print(t)
        t = blockchain.transactions.fulfill(t, private_keys=user['private_key'])
        signed_tx = blockchain.transactions.send_commit(t)
        return signed_tx['id']


    @classmethod
    def get_last_transaction(cls, equipmentId):
        transaction = bdb.transactions.find_one({'asset.id': equipmentId}, sort=[('_id', -1)])
        if not transaction:
            # Could be a creation transaction without asset id but with id equal to it
            transaction = bdb.transactions.find_one({'id': equipmentId})
        if not transaction:
            raise AppException('transaction_not_found', f'No transactions for asset `{equipmentId}`')
        return transaction

    @classmethod
    def get_actual_metadata(cls, lastTransaction):
        return bdb.metadata.find_one({'id': lastTransaction['id']})['metadata']

    @classmethod
    def ids_for_sale(cls):
        metadatasForSale = bdb.metadata.find({"metadata.for_sale": True})
        transactionIds = [m['id'] for m in metadatasForSale]
        transactions = bdb.transactions.find({"id": {"$in": transactionIds}})
        assetIds = set([t.get('asset') and t['asset']['id'] or t['id'] for t in transactions])
        ids = []
        for eqid in assetIds:
            t = cls.get_last_transaction(eqid)
            md = cls.get_actual_metadata(t)
            if md.get("for_sale"):
                ids.append(eqid)
        return ids

    @classmethod
    def all_ids(cls):
        return [x['id'] for x in bdb.assets.find()]

    @classmethod
    def get_history(cls, eqid):
        transactions = bdb.transactions.aggregate([
            {"$match": {"$or": [{'asset.id': eqid}, {"id": eqid}],}},
            {"$lookup": {"from": "metadata", "localField": "id", "foreignField": "id", "as": "metadata"}},
            {"$unwind": "$metadata"}
        ])
        return [t['metadata']['metadata'] for t in transactions]
