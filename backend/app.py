from flask import Flask, request, jsonify

from flask_cors import CORS, cross_origin
from pymongo import MongoClient
from functools import wraps
import time
import json

from .mongoflask import MongoJSONEncoder, ObjectIdConverter
from .bdbhelpers import Equipment, AppException

app = Flask(__name__)
CORS(app, supports_credentials=True, resources={r"/*": {"origins": "*"}})
app.json_encoder = MongoJSONEncoder
app.url_map.converters['objectid'] = ObjectIdConverter
mongo_client = MongoClient('mongodb://localhost:27017/')
db = mongo_client.sumo

def with_user(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        token = request.cookies.get('token')
        # user = db.users.find_one({'token': token})
        user = db.users.find_one({})
        if not user:
            raise AppException('auth_error', f'User not found for token `{token}`')
        kwargs['user'] = user
        return f(*args, **kwargs)
    return decorated_function


@app.route('/')
def index():
    return 'Hello, World!'

@app.route('/equipment')
@with_user
def equipmentForSale(user):
    ids = Equipment.ids_for_sale()
    descriptions = []
    for eqid in ids:
        t = Equipment.get_last_transaction(eqid)
        metadata = Equipment.get_actual_metadata(t)
        descriptions.append({**metadata, "id": eqid})
    return jsonify(descriptions)

@app.route('/equipment/all')
@with_user
def allEquipment(user):
    ids = Equipment.all_ids()
    descriptions = []
    for eqid in ids:
        t = Equipment.get_last_transaction(eqid)
        metadata = Equipment.get_actual_metadata(t)
        descriptions.append({**metadata, "id": eqid})

    return jsonify(descriptions)

@app.route('/equipment/create', methods=["POST"])
@with_user
def createEquipment(user):
    contents = request.get_json()
    title = contents.get('title')
    assert title
    serial_no = contents.get('serial_no')
    assert serial_no
    return jsonify(Equipment.create(user, title, serial_no, contents))


@app.route('/equipment/checkTransactionCommited/<txid>')
def checkTransactionCommited(txid):
    return jsonify(Equipment.is_transaction_commited(txid))


@app.route('/equipment/<eqid>', methods=["GET"])
@with_user
def getEquipmentDetails(user, eqid):
    t = Equipment.get_last_transaction(eqid)
    metadata = Equipment.get_actual_metadata(t)
    history = Equipment.get_history(eqid)
    betterHistory = []
    for i, item in enumerate(history):
        op = item.get('op')
        reason = item.get('reason')
        if not op:
            if i == 0:
                op = 'create'
            else:
                op = 'change_description'

        if len(betterHistory) and betterHistory[-1]["op"] == 'change_description' \
                and op == 'change_description':
            betterHistory[-1]["date"] = item["date"]
        else:
            betterHistory.append({**item,
                "op": op, "reason": reason
            })
    return jsonify({**metadata, "history": betterHistory})


@app.route('/equipment/<eqid>', methods=["POST"])
@with_user
def updateMetadata(user, eqid):
    contents = request.get_json()
    txid = Equipment.update_metadata(user, eqid, contents)
    return jsonify({"transactionId": txid})


@app.errorhandler(AppException)
def handle_bad_request(e):
    return jsonify({
        'error': True,
        'errorCode': e.errorCode,
        'debugDescription': str(e)
    })

app.register_error_handler(400, handle_bad_request)
